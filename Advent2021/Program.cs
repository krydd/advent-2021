﻿using System;
using System.Diagnostics;
using Advent2021.Tasks;

namespace Advent2021
{
    class Program
    {
        static void Main(string[] args)
        {
            var sw = Stopwatch.StartNew();

            new Day22().Part2();

            Console.WriteLine($"\n(Took {sw.ElapsedMilliseconds} ms)");
        }
    }
}
