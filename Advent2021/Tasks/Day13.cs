using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day13
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day13.txt");

            var points = new HashSet<(int x, int y)>();
            var fold = "";
            var foldParse = false;
            
            foreach (var input in inputs)
            {
                if (input == "")
                {
                    foldParse = true;
                    continue;
                }

                if (foldParse)
                {
                    fold = input.Split("fold along ")[1];
                    break;
                }
                else
                {
                    var coords = input.Split(",").Select(int.Parse).ToList();
                    points.Add((coords[0], coords[1]));
                }
            }

            var result = Fold(points, fold.StartsWith("x"), int.Parse(fold.Split("=")[1])).Count;

            Console.WriteLine($"Ans: {result}");
        }
        
        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day13.txt");

            var points = new HashSet<(int x, int y)>();
            var folds = new List<string>();
            var foldParse = false;
            
            foreach (var input in inputs)
            {
                if (input == "")
                {
                    foldParse = true;
                    continue;
                }

                if (foldParse)
                {
                    folds.Add(input.Split("fold along ")[1]);
                }
                else
                {
                    var coords = input.Split(",").Select(int.Parse).ToList();
                    points.Add((coords[0], coords[1]));
                }
            }

            points = folds.Aggregate(points, (current, fold) => Fold(current, fold.StartsWith("x"), int.Parse(fold.Split("=")[1])));

            Print(points);
        }

        private HashSet<(int x, int y)> Fold(HashSet<(int x, int y)> points, bool vertical, int coord)
        {
            var newPoints = new HashSet<(int, int)>();
            foreach (var point in points)
            {
                if (vertical)
                {
                    if (point.x < coord)
                    {
                        newPoints.Add(point);
                    }
                    else
                    {
                        var newX = point.x - (point.x - coord) * 2;
                        newPoints.Add((newX, point.y));
                    }
                }
                else
                {
                    if (point.y < coord)
                    {
                        newPoints.Add(point);
                    }
                    else
                    {
                        var newY = point.y - (point.y - coord) * 2;
                        newPoints.Add((point.x, newY));
                    }
                }
            }

            return newPoints;
        }

        private void Print(HashSet<(int x, int y)> points)
        {
            for (var y = points.Min(p => p.y); y <= points.Max(p => p.y); y++)
            {
                for (var x = points.Min(p => p.x); x <= points.Max(p => p.x); x++)
                {
                    Console.Write($"{(points.Contains((x, y)) ? "#" : ".")}");
                }

                Console.WriteLine();
            }
        }
    }
}
