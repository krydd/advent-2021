using System;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day18
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day18.txt").Select(Pair.ParsePair).ToList();
            var p1 = inputs.First();
            var finalPair = inputs.Skip(1).Aggregate(p1, (pair1, pair2) => pair1.Add(pair2));
            
            Console.WriteLine($"Ans: {finalPair.Magnitude()}");
        }
        
        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day18.txt").Select(Pair.ParsePair).ToList();

            var maxMagnitude = 0;
            for (var i = 0; i < inputs.Count; i++)
            {
                var left = inputs[i];
                for (var j = 0; j < inputs.Count; j++)
                {
                    if (j == i)
                    {
                        continue;
                    }

                    var magnitude = left.Add(inputs[j]).Magnitude();
                    maxMagnitude = Math.Max(maxMagnitude, magnitude);
                }
            }

            Console.WriteLine($"Ans: {maxMagnitude}");
        }

        private class Pair
        {
            public Pair(int leftValue, int rightValue, Pair parent = null)
            {
                Parent = parent;
                LeftValue = leftValue;
                RightValue = rightValue;
            }

            public Pair(int leftValue, Pair rightPair, Pair parent = null)
            {
                Parent = parent;
                LeftValue = leftValue;
                RightPair = rightPair;

                rightPair.Parent = this;
            }

            public Pair(Pair leftPair, int rightValue, Pair parent = null)
            {
                Parent = parent;
                LeftPair = leftPair;
                RightValue = rightValue;

                leftPair.Parent = this;
            }

            public Pair(Pair leftPair, Pair rightPair, Pair parent = null)
            {
                Parent = parent;
                LeftPair = leftPair;
                RightPair = rightPair;

                leftPair.Parent = this;
                rightPair.Parent = this;
            }

            public Pair Parent { get; set; }

            public int? LeftValue { get; set; }

            public int? RightValue { get; set; }

            public Pair LeftPair { get; set; }

            public Pair RightPair { get; set; }

            public Pair Add(Pair otherRightPair)
            {
                var sumPair = new Pair(this, otherRightPair);
                Parent = sumPair;
                otherRightPair.Parent = sumPair;

                // Console.WriteLine($"after addition: {sumPair.ToPrint()}");

                return sumPair.Reduce();
            }

            private Pair Reduce()
            {
                var pairToWorkOn = this;

                var actionPerformed = true;
                while (actionPerformed)
                {
                    var newPair = Explode(pairToWorkOn);
                    if (newPair != null)
                    {
                        pairToWorkOn = newPair;
                        // Console.WriteLine($"After explode: {pairToWorkOn.ToPrint()}");
                        continue;
                    }

                    actionPerformed = Split(pairToWorkOn);
                    if (actionPerformed)
                    {
                        // Console.WriteLine($"After split: {pairToWorkOn.ToPrint()}");
                    }
                }

                return pairToWorkOn;
            }

            private Pair Explode(Pair pair)
            {
                var pairAsString = pair.ToPrint();
                var bracketCount = 0;
                var count = 0;
                var prevNumberPos = -1;
                var prevNumberSize = -1;
                var startOfExplodingPair = -1;
                foreach (var c in pairAsString.ToCharArray())
                {
                    if (c == '[')
                    {
                        bracketCount++;
                    }
                    else if (c == ']')
                    {
                        bracketCount--;
                    }
                    else if (c >= '0' && c <= '9' && startOfExplodingPair < 0)
                    {
                        if (prevNumberPos == count - prevNumberSize)
                        {
                            prevNumberSize++;
                        }
                        else
                        {
                            prevNumberPos = count;
                            prevNumberSize = 1;
                        }
                    }

                    if (bracketCount == 5 && startOfExplodingPair < 0)
                    {
                        startOfExplodingPair = count;
                    }

                    if (c == ']' && startOfExplodingPair > -1)
                    {
                        var explodingPair = pairAsString.Substring(startOfExplodingPair, count - startOfExplodingPair + 1);
                        var values = explodingPair.Substring(1, explodingPair.Length - 2).Split(",").Select(int.Parse).ToList();

                        var nextNumberPos = -1;
                        var nextNumberSize = -1;
                        for (var i = count; i < pairAsString.Length; i++)
                        {
                            if (pairAsString.ElementAt(i) >= '0' && pairAsString.ElementAt(i) <= '9')
                            {
                                if (nextNumberPos == -1)
                                {
                                    nextNumberPos = i;
                                    nextNumberSize = 1;
                                }
                                else if (nextNumberPos == i - nextNumberSize)
                                {
                                    nextNumberSize++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }

                        var resultString = "";
                        if (prevNumberPos > -1)
                        {
                            resultString += pairAsString.Substring(0, prevNumberPos);
                            resultString += values[0] + int.Parse(pairAsString.Substring(prevNumberPos, prevNumberSize));
                            resultString += pairAsString.Substring(prevNumberPos + prevNumberSize, startOfExplodingPair - prevNumberPos - prevNumberSize);
                        }
                        else
                        {
                            resultString += pairAsString.Substring(0, startOfExplodingPair);
                        }

                        resultString += "0";

                        if (nextNumberPos > -1)
                        {
                            resultString += pairAsString.Substring(startOfExplodingPair + explodingPair.Length, nextNumberPos - startOfExplodingPair - explodingPair.Length);
                            resultString += values[1] + int.Parse(pairAsString.Substring(nextNumberPos, nextNumberSize));
                            resultString += pairAsString.Substring(nextNumberPos + nextNumberSize);
                        }
                        else
                        {
                            resultString += pairAsString.Substring(startOfExplodingPair + explodingPair.Length);
                        }

                        return ParsePair(resultString);
                    }

                    count++;
                }

                return null;
            }

            private static bool Split(Pair pair)
            {
                if (pair.LeftValue is > 9) // Split left
                {
                    pair.LeftPair = new Pair(pair.LeftValue.Value / 2, (int)Math.Ceiling(pair.LeftValue.Value / 2m));
                    pair.LeftValue = null;
                    return true;
                }
                
                if (pair.LeftPair != null)
                {
                    var found = Split(pair.LeftPair);
                    if (found)
                    {
                        return true;
                    }
                }

                if (pair.RightPair != null)
                {
                    var found = Split(pair.RightPair);
                    if (found)
                    {
                        return true;
                    }
                }

                if (pair.RightValue is > 9) // Split right
                {
                    pair.RightPair = new Pair(pair.RightValue.Value / 2, (int)Math.Ceiling(pair.RightValue.Value / 2m));
                    pair.RightValue = null;
                    return true;
                }

                return false;
            }

            public int Magnitude()
            {
                return 3 * (LeftValue ?? LeftPair.Magnitude()) + 2 * (RightValue ?? RightPair.Magnitude());
            }

            public string ToPrint()
            {
                var p = "[";
                if (LeftValue != null)
                {
                    p += LeftValue.ToString();
                }
                else
                {
                    p += LeftPair.ToPrint();
                }

                p += ",";

                if (RightValue != null)
                {
                    p += RightValue.ToString();
                }
                else
                {
                    p += RightPair.ToPrint();
                }

                p += "]";

                return p;
            }

            public int Depth()
            {
                if (Parent != null)
                {
                    return 1 + Parent.Depth();
                }

                return 1;
            }

            public static Pair ParsePair(string str)
            {
                var isLeft = true;
                var bracketCount = 0;

                var leftValue = "";
                var rightValue = "";
                var leftPairStr = "";
                var rightPairStr = "";

                foreach (var c in str.ToCharArray())
                {
                    if (c == ',' && bracketCount == 1)
                    {
                        isLeft = false;
                    }
                    else if (bracketCount == 1 && int.TryParse(c.ToString(), out var nr))
                    {
                        if (isLeft)
                        {
                            leftValue += nr.ToString();
                        }
                        else
                        {
                            rightValue += nr.ToString();
                        }
                    }
                    else
                    {
                        if (bracketCount > 0)
                        {
                            if (isLeft)
                            {
                                leftPairStr += c.ToString();
                            }
                            else
                            {
                                rightPairStr += c.ToString();
                            }
                        }

                        if (c == '[')
                        {
                            bracketCount++;
                        }
                        else if (c == ']')
                        {
                            bracketCount--;
                        }
                    }
                }

                rightPairStr = rightPairStr == "" ? rightPairStr : rightPairStr[..^1];

                if (leftValue != "")
                {
                    if (rightValue != "")
                    {
                        return new Pair(int.Parse(leftValue), int.Parse(rightValue));
                    }

                    return new Pair(int.Parse(leftValue), ParsePair(rightPairStr));
                }

                if (rightValue != "")
                {
                    return new Pair(ParsePair(leftPairStr), int.Parse(rightValue));
                }

                return new Pair(ParsePair(leftPairStr), ParsePair(rightPairStr));
            }
        }
    }
}
