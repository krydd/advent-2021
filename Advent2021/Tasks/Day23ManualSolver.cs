using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day23ManualSolver
    {
        private const char Empty = '.';
        private readonly Dictionary<char, int> costs = new()
        {
            ['A'] = 1,
            ['B'] = 10,
            ['C'] = 100,
            ['D'] = 1000
        };
        
        private (int x, int y) selected = (0, 0);
        private int totalCost = 0;
        private char[] hallway = new char[11];
        private readonly char[,] caves = new char[4, 4];
        
        public void Start()
        {
            var inputs = File.ReadLines("Data\\day23.txt").ToList(); // 48304

            Reset(inputs);

            var key = ConsoleKey.A;
            while (key != ConsoleKey.Q)
            {
                Print();
                
                key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.W when selected.y > 0:
                        selected.y--;
                        break;
                    case ConsoleKey.S:
                        if (selected.y < 4 && selected.x is 2 or 4 or 6 or 8)
                        {
                            selected.y++;
                        }
                        break;
                    case ConsoleKey.A:
                        if (selected.y == 0 && selected.x > 0)
                        {
                            selected.x--;
                        }

                        if (selected.y > 0 && selected.x > 2)
                        {
                            selected.x -= 2;
                        }
                        break;
                    case ConsoleKey.D:
                        if (selected.y == 0 && selected.x < 10)
                        {
                            selected.x++;
                        }

                        if (selected.y > 0 && selected.x < 8)
                        {
                            selected.x += 2;
                        }
                        break;
                    case ConsoleKey.UpArrow:
                        if (selected.y > 0 && CharAt(selected.x, selected.y) != Empty && CharAt(selected.x, selected.y - 1) == Empty)
                        {
                            AddToCost();
                            SetChar(selected.x, selected.y - 1, CharAt(selected.x, selected.y));
                            SetChar(selected.x, selected.y, Empty);
                            
                            selected.y--;
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        if (selected.y < 4 && selected.x is 2 or 4 or 6 or 8 && CharAt(selected.x, selected.y) != Empty && CharAt(selected.x, selected.y + 1) == Empty)
                        {
                            AddToCost();
                            SetChar(selected.x, selected.y + 1, CharAt(selected.x, selected.y));
                            SetChar(selected.x, selected.y, Empty);
                            
                            selected.y++;
                        }
                        break;
                    case ConsoleKey.LeftArrow:
                        if (selected.y == 0 && selected.x > 0 && CharAt(selected.x, selected.y) != Empty && CharAt(selected.x - 1, selected.y) == Empty)
                        {
                            AddToCost();
                            SetChar(selected.x - 1, selected.y, CharAt(selected.x, selected.y));
                            SetChar(selected.x, selected.y, Empty);
                            
                            selected.x--;
                        }
                        break;
                    case ConsoleKey.RightArrow:
                        if (selected.y == 0 && selected.x < 10 && CharAt(selected.x, selected.y) != Empty && CharAt(selected.x + 1, selected.y) == Empty)
                        {
                            AddToCost();
                            SetChar(selected.x + 1, selected.y, CharAt(selected.x, selected.y));
                            SetChar(selected.x, selected.y, Empty);
                            
                            selected.x++;
                        }
                        break;
                    case ConsoleKey.R:
                        Reset(inputs);
                        selected = (0, 0);
                        totalCost = 0;
                        break;
                }
            }
        }

        private void Reset(List<string> inputs)
        {
            hallway = new[] { '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' };
            // hallway = new[] { 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A' };

            var count = 0;
            foreach (var s in inputs.Skip(2).Take(4).Select(s => s.Trim().Trim('#')))
            {
                foreach (var letter in s.Split("#"))
                {
                    caves[count % 4, count / 4] = letter.First();
                    count++;
                }
            }
        }

        private void Print()
        {
            Console.Clear();
            Console.WriteLine("#############");
            
            var hall = string.Join("", Enumerable.Range(0, 11).Select(ToRightHallwayChar));
            Console.Write("#");
            Console.Write(hall.Substring(0, 2));
            PrintCaveEntrance(hall, 2);
            Console.Write(hall.ElementAt(3));
            PrintCaveEntrance(hall, 4);
            Console.Write(hall.ElementAt(5));
            PrintCaveEntrance(hall, 6);
            Console.Write(hall.ElementAt(7));
            PrintCaveEntrance(hall, 8);
            Console.Write(hall.Substring(9) + "#\n");

            Console.WriteLine($"###{ToRightCaveChar(0, 0)}#{ToRightCaveChar(1, 0)}#{ToRightCaveChar(2, 0)}#{ToRightCaveChar(3, 0)}###");
            for (var i = 1; i < 4; i++)
            {
                Console.WriteLine($"  #{ToRightCaveChar(0, i)}#{ToRightCaveChar(1, i)}#{ToRightCaveChar(2, i)}#{ToRightCaveChar(3, i)}#");
            }
            Console.WriteLine("  #########  ");
            Console.WriteLine();
            Console.WriteLine($"Cost: {totalCost}");
        }

        private void PrintCaveEntrance(string hall, int index)
        {
            if (hall.ElementAt(index) != Empty)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(hall.ElementAt(index));
                Console.ResetColor();
            }
            else
            {
                Console.Write(hall.ElementAt(index));
            }
        }

        private void AddToCost()
        {
            totalCost += costs[CharAt(selected.x, selected.y)];
        }

        private char CharAt(int x, int y)
        {
            if (y == 0)
            {
                return hallway[x];
            }
            else
            {
                if (x is 2 or 4 or 6 or 8)
                {
                    return caves[x / 2 - 1, y - 1];
                }
            }

            throw new ArgumentOutOfRangeException($"Out of bounds: {x},{y}");
        }

        private void SetChar(int x, int y, char c)
        {
            if (y == 0)
            {
                hallway[x] = c;
                return;
            }

            if (x is 2 or 4 or 6 or 8)
            {
                caves[x / 2 - 1, y - 1] = c;
                return;
            }

            throw new ArgumentOutOfRangeException($"Out of bounds: {x},{y}");
        }

        private char ToRightHallwayChar(int index)
        {
            if ((index, 0) == selected)
            {
                return hallway[index] == Empty ? Empty : hallway[index].ToString().ToLower().First();
            }

            return hallway[index];
        }

        private char ToRightCaveChar(int cave, int depth)
        {
            if ((cave * 2 + 2, depth + 1) == selected)
            {
                return caves[cave, depth] == Empty ? Empty : caves[cave, depth].ToString().ToLower().First();
            }

            return caves[cave, depth];
        }
    }
}
