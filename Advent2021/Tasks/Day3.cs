using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day3
    {
        public void Part1()
        {
            var lines = File.ReadLines("Data\\day3.txt").ToList();
            var numBits = lines.First().Length;
            var rows = lines.Count;

            var ones = new int[numBits].ToList();
            
            foreach (var line in lines)
            {
                var count = 0;
                foreach (var c in line.ToCharArray())
                {
                    if (c == '1')
                    {
                        ones[count]++;
                    }

                    count++;
                }
            }

            var finalBits = ones.Select(num => num > rows / 2 ? 1 : 0).ToList();

            var result1 = Convert.ToInt32(string.Join("", finalBits), 2);
            var result2 = Convert.ToInt32(string.Join("", finalBits.Select(c => c == 1 ? 0 : 1)), 2);

            Console.WriteLine($"Ans: {result1 * result2}");
        }

        public void Part2()
        {
            var lines = File.ReadLines("Data\\day3.txt").ToList();

            var copy = lines.ToList();
            var pos = 0;
            while (copy.Count > 1)
            {
                var (zeroes, ones) = CountThem(copy, pos);
                if (ones >= zeroes)
                {
                    copy = copy.Where(row => row.ElementAt(pos) == '1').ToList();
                }
                else
                {
                    copy = copy.Where(row => row.ElementAt(pos) == '0').ToList();
                }
                
                pos++;
            }

            var oxygenRating = Convert.ToInt32(copy.Single(), 2);
            
            copy = lines.ToList();
            pos = 0;
            while (copy.Count > 1)
            {
                var (zeroes, ones) = CountThem(copy, pos);
                if (ones < zeroes)
                {
                    copy = copy.Where(row => row.ElementAt(pos) == '1').ToList();
                }
                else
                {
                    copy = copy.Where(row => row.ElementAt(pos) == '0').ToList();
                }
                
                pos++;
            }

            var co2ScrubberRating = Convert.ToInt32(copy.Single(), 2);

            Console.WriteLine($"Ans: {oxygenRating * co2ScrubberRating}");
        }

        private (int zeroes, int ones) CountThem(List<string> list, int pos)
        {
            var zeroes = 0;
            var ones = 0;
            foreach (var c in list.Select(row => row.ElementAt(pos)))
            {
                if (c == '0')
                {
                    zeroes++;
                }
                else
                {
                    ones++;
                }
            }

            return (zeroes, ones);
        }
    }
}
