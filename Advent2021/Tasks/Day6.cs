using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day6
    {
        public void Part1()
        {
            var fish = File.ReadAllText("Data\\day6.txt").Split(",").Select(byte.Parse).ToList();

            const int days = 256;

            for (var i = 0; i < days; i++)
            {
                var newFish = 0;
                for (var j = 0; j < fish.Count; j++)
                {
                    if (fish[j] == 0)
                    {
                        fish[j] = 6;
                        newFish++;
                    }
                    else
                    {
                        fish[j]--;
                    }
                }
                
                for (var k = 0; k < newFish; k++)
                {
                    fish.Add(8);
                }
            }

            Console.WriteLine($"Ans: {fish.Count}");
        }
        
        public void Part2()
        {
            var fish = File.ReadAllText("Data\\day6.txt").Split(",").Select(int.Parse)
                .GroupBy(grp => grp)
                .ToDictionary(grp => grp.Key, grp => (long) grp.Count());

            const int days = 256;

            for (var i = 0; i < days; i++)
            {
                var next = new Dictionary<int, long>();
                foreach (var (d, num) in fish)
                {
                    if (d == 0)
                    {
                        next[8] = num;
                        if (next.ContainsKey(6))
                        {
                            next[6] += num;
                        }
                        else
                        {
                            next[6] = num;
                        }
                    }
                    else
                    {
                        if (next.ContainsKey(d - 1))
                        {
                            next[d - 1] += num;
                        }
                        else
                        {
                            next[d - 1] = num;
                        }
                    }
                }

                fish = next;
            }

            Console.WriteLine($"Ans: {fish.Values.Sum()}");
        }
    }
}
