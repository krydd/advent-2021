using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day24
    {
        public void Part1()
        {
            var program = File.ReadLines("Data\\day24.txt").ToList();

            var alu = new Alu();

            // "99929995111111"
            var result = alu.IsValid(program, "11118151637112");
        }

        public void Part2()
        {
            var program = File.ReadLines("Data\\day24.txt").ToList();

            var alu = new Alu();

            var result = alu.IsValid(program, "74929995999389");
        }

        private class Alu
        {
            public long IsValid(List<string> program, string input)
            {
                var vars = new Dictionary<string, long>
                {
                    ["w"] = 0,
                    ["x"] = 0,
                    ["y"] = 0,
                    ["z"] = 0
                };

                var inputCount = 0;
                var pc = 0;

                var states = new Dictionary<(long w, long x, long y, long z), int>();

                while (pc < program.Count)
                {
                    var key = (vars["w"], vars["x"], vars["y"], vars["z"]);
                    if (states.ContainsKey(key))
                    {
                        states[key] = pc;
                    }
                    else
                    {
                        states.Add(key, pc);
                    }

                    var instruction = program[pc].Split(" ").ToArray();
                    var op = instruction[0];
                    var a = instruction[1];
                    var b = op == "inp" ? null : instruction[2];
                    var nr = 0;
                    switch (op)
                    {
                        case "inp":
                            vars[a] = int.Parse(input.Substring(inputCount, 1));
                            inputCount++;
                            break;
                        case "add":
                            if (int.TryParse(b, out nr))
                            {
                                vars[a] += nr;
                            }
                            else
                            {
                                vars[a] += vars[b];
                            }

                            break;
                        case "mul":
                            if (int.TryParse(b, out nr))
                            {
                                vars[a] *= nr;
                            }
                            else
                            {
                                vars[a] *= vars[b];
                            }

                            break;
                        case "div":
                            if (int.TryParse(b, out nr))
                            {
                                vars[a] /= nr;
                            }
                            else
                            {
                                vars[a] /= vars[b];
                            }

                            break;
                        case "mod":
                            if (int.TryParse(b, out nr))
                            {
                                vars[a] %= nr;
                            }
                            else
                            {
                                vars[a] %= vars[b];
                            }

                            break;
                        case "eql":
                            if (int.TryParse(b, out nr))
                            {
                                vars[a] = vars[a] == nr ? 1 : 0;
                            }
                            else
                            {
                                vars[a] = vars[a] == vars[b] ? 1 : 0;
                            }

                            break;
                        default:
                            throw new Exception($"Unknown instruction: {op}");
                    }

                    pc++;
                }

                PrintVars(vars);

                return vars["z"];
            }

            private void PrintVars(Dictionary<string, long> vars)
            {
                Console.WriteLine($"w: {vars["w"]}");
                Console.WriteLine($"x: {vars["x"]}");
                Console.WriteLine($"y: {vars["y"]}");
                Console.WriteLine($"z: {vars["z"]}");
            }
        }
    }
}
