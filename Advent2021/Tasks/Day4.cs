using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day4
    {
        public void Part1()
        {
            var lines = File.ReadLines("Data\\day4.txt").ToList();
            var balls = lines[0].Split(",").Select(int.Parse).ToList();

            var boards = new List<Board>();
            var currentBoard = new Board();
            foreach (var line in lines.Skip(2))
            {
                if (line == "")
                {
                    boards.Add(currentBoard);
                    currentBoard = new Board();
                    continue;
                }

                var row = line.Split(" ").Where(s => s != "").Select(int.Parse).ToList();
                currentBoard.AddRow(row);
            }

            boards.Add(currentBoard);

            foreach (var ball in balls)
            {
                boards.ForEach(b => b.MarkNumber(ball));

                if (boards.Any(b => b.HasBingo()))
                {
                    var winner = boards.Single(b => b.HasBingo());
                    var sum = winner.SumUnmarked();
                    Console.WriteLine($"Ans: {sum * ball}");
                    break;
                }
            }
        }

        public void Part2()
        {
            var lines = File.ReadLines("Data\\day4.txt").ToList();
            var balls = lines[0].Split(",").Select(int.Parse).ToList();

            var boards = new List<Board>();
            var currentBoard = new Board();
            foreach (var line in lines.Skip(2))
            {
                if (line == "")
                {
                    boards.Add(currentBoard);
                    currentBoard = new Board();
                    continue;
                }

                var row = line.Split(" ").Where(s => s != "").Select(int.Parse).ToList();
                currentBoard.AddRow(row);
            }

            boards.Add(currentBoard);

            foreach (var ball in balls)
            {
                boards.ForEach(b => b.MarkNumber(ball));

                if (boards.Count > 1)
                {
                    boards.RemoveAll(b => b.HasBingo());
                }
                else
                {
                    var last = boards.Single();
                    var sum = last.SumUnmarked();
                    Console.WriteLine($"Ans: {sum * ball}");
                    break;
                }
            }
        }

        private class Board
        {
            private int currentRow = 0;
            private readonly int[,] numbers = new int[5, 5];
            private readonly bool[,] marked = new bool[5, 5];

            public void AddRow(List<int> row)
            {
                for (var i = 0; i < row.Count; i++)
                {
                    numbers[i, currentRow] = row[i];
                }

                currentRow++;
            }

            public void MarkNumber(int number)
            {
                if (!numbers.Cast<int>().Contains(number))
                {
                    return;
                }

                for (var x = 0; x < 5; x++)
                {
                    for (var y = 0; y < 5; y++)
                    {
                        if (numbers[x, y] == number)
                        {
                            marked[x, y] = true;
                            return;
                        }
                    }
                }
            }

            public bool HasBingo()
            {
                for (var x = 0; x < 5; x++)
                {
                    var row = new List<bool>();
                    for (var y = 0; y < 5; y++)
                    {
                        row.Add(marked[x, y]);
                    }

                    if (row.All(e => e))
                    {
                        return true;
                    }
                }

                for (var y = 0; y < 5; y++)
                {
                    var col = new List<bool>();
                    for (var x = 0; x < 5; x++)
                    {
                        col.Add(marked[x, y]);
                    }

                    if (col.All(e => e))
                    {
                        return true;
                    }
                }

                return false;
            }

            public int SumUnmarked()
            {
                var sum = 0;
                for (var x = 0; x < 5; x++)
                {
                    for (var y = 0; y < 5; y++)
                    {
                        if (!marked[x, y])
                        {
                            sum += numbers[x, y];
                        }
                    }
                }

                return sum;
            }
        }
    }
}
