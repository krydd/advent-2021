using System;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day7
    {
        public void Part1()
        {
            var heights = File.ReadAllText("Data\\day7.txt").Split(",").Select(int.Parse).OrderBy(i => i).ToList();

            var min = heights.First();
            var max = heights.Last();

            var result = int.MaxValue;
            for (var targetHeight = min; targetHeight <= max; targetHeight++)
            {
                var sum = 0;
                foreach (var height in heights)
                {
                    sum += Math.Abs(height - targetHeight);
                }

                if (sum < result)
                {
                    result = sum;
                }
            }

            Console.WriteLine($"Ans: {result}");
        }
        
        public void Part2()
        {
            var heights = File.ReadAllText("Data\\day7.txt").Split(",").Select(int.Parse).OrderBy(i => i).ToList();

            var min = heights.First();
            var max = heights.Last();

            var result = int.MaxValue;
            for (var targetHeight = min; targetHeight <= max; targetHeight++)
            {
                var sum = 0;
                foreach (var height in heights)
                {
                    var distance = Math.Abs(height - targetHeight);
                    sum += Enumerable.Range(1, distance).Sum();
                }

                if (sum < result)
                {
                    result = sum;
                }
            }

            Console.WriteLine($"Ans: {result}");
        }
    }
}
