using System;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day17
    {
        public void Part1()
        {
            var inputs = File.ReadAllText("Data\\day17.txt").Split("target area: ")[1].Split(", ");
            var xValues = inputs.Single(s => s.StartsWith("x")).Split("x=")[1].Split("..").Select(int.Parse).ToList();
            var yValues = inputs.Single(s => s.StartsWith("y")).Split("y=")[1].Split("..").Select(int.Parse).ToList();

            var targetXMin = xValues[0];
            var targetXMax = xValues[1];
            var targetYMin = yValues[0];
            var targetYMax = yValues[1];

            Console.WriteLine($"x: {targetXMin} -> {targetXMax}, y: {targetYMin} -> {targetYMax}");

            const int dyMax = 200;
            const int dxMax = 200;

            var bestY = 0;
            for (var dyStart = 0; dyStart < dyMax; dyStart++)
            {
                for (var dxStart = 10; dxStart < dxMax; dxStart++)
                {
                    var x = 0;
                    var y = 0;
                    var dx = dxStart;
                    var dy = dyStart;
                    var top = 0;
                    while (CheckLocation(x, y, targetXMin, targetXMax, targetYMin, targetYMax) == Location.BeforeTarget)
                    {
                        x += dx;
                        y += dy;
                        dx += dx == 0 ? 0 : dx > 0 ? -1 : 1;
                        dy -= 1;

                        top = Math.Max(top, y);
                    }

                    var result = CheckLocation(x, y, targetXMin, targetXMax, targetYMin, targetYMax);
                    if (result == Location.InTargetArea)
                    {
                        bestY = Math.Max(bestY, top);
                    }
                }
            }

            Console.WriteLine($"Ans: {bestY}");
        }
        
        public void Part2()
        {
            var inputs = File.ReadAllText("Data\\day17.txt").Split("target area: ")[1].Split(", ");
            var xValues = inputs.Single(s => s.StartsWith("x")).Split("x=")[1].Split("..").Select(int.Parse).ToList();
            var yValues = inputs.Single(s => s.StartsWith("y")).Split("y=")[1].Split("..").Select(int.Parse).ToList();

            var targetXMin = xValues[0];
            var targetXMax = xValues[1];
            var targetYMin = yValues[0];
            var targetYMax = yValues[1];

            Console.WriteLine($"x: {targetXMin} -> {targetXMax}, y: {targetYMin} -> {targetYMax}");

            const int dxMax = 1000;
            const int dyMin = -1000;
            const int dyMax = 1000;

            var sum = 0;
            for (var dyStart = dyMin; dyStart < dyMax; dyStart++)
            {
                for (var dxStart = 1; dxStart < dxMax; dxStart++)
                {
                    var x = 0;
                    var y = 0;
                    var dx = dxStart;
                    var dy = dyStart;
                    while (CheckLocation(x, y, targetXMin, targetXMax, targetYMin, targetYMax) == Location.BeforeTarget)
                    {
                        x += dx;
                        y += dy;
                        dx += dx == 0 ? 0 : dx > 0 ? -1 : 1;
                        dy -= 1;
                    }

                    var result = CheckLocation(x, y, targetXMin, targetXMax, targetYMin, targetYMax);
                    if (result == Location.InTargetArea)
                    {
                        sum++;
                    }
                }
            }

            Console.WriteLine($"Ans: {sum}");
        }

        private Location CheckLocation(int x, int y, int targetXMin, int targetXMax, int targetYMin, int targetYMax)
        {
            if (targetXMin <= x && x <= targetXMax && targetYMin <= y && y <= targetYMax)
            {
                return Location.InTargetArea;
            }

            if (x > targetXMax || y < targetYMin)
            {
                return Location.AfterTarget;
            }

            return Location.BeforeTarget;
        }

        private enum Location
        {
            BeforeTarget,
            InTargetArea,
            AfterTarget
        }
    }
}
