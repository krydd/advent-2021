using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day25
    {
        private enum SCDirection
        {
            East,
            South
        }
        
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day25.txt").ToList();

            var grid = ExtractGrid(inputs);
            var maxX = grid.Keys.Select(k => k.x).Max();
            var maxY = grid.Keys.Select(k => k.y).Max();

            var steps = 0;
            var action = true;
            while (action)
            {
                steps++;
                action = false;
                
                var newGrid = new Dictionary<(int x, int y), SCDirection>();

                foreach (var pair in grid.Where(pair => pair.Value == SCDirection.East))
                {
                    var x = pair.Key.x;
                    var y = pair.Key.y;
                    var newX = x + 1 > maxX ? 0 : x + 1;
                    if (grid.ContainsKey((newX, y)))
                    {
                        newGrid[(x, y)] = SCDirection.East;
                    }
                    else
                    {
                        action = true;
                        newGrid[(newX, y)] = SCDirection.East;
                    }
                }
                
                foreach (var pair in grid.Where(pair => pair.Value == SCDirection.South))
                {
                    var x = pair.Key.x;
                    var y = pair.Key.y;
                    var newY = y + 1 > maxY ? 0 : y + 1;
                    if (newGrid.ContainsKey((x, newY)) || (grid.ContainsKey((x, newY)) && grid[(x, newY)] == SCDirection.South))
                    {
                        newGrid[(x, y)] = SCDirection.South;
                    }
                    else
                    {
                        action = true;
                        newGrid[(x, newY)] = SCDirection.South;
                    }
                }
                
                grid = newGrid;
            }

            Console.WriteLine($"Ans: {steps}");
        }

        private Dictionary<(int x, int y), SCDirection> ExtractGrid(List<string> inputs)
        {
            var grid = new Dictionary<(int x, int y), SCDirection>();

            var y = 0;
            foreach (var input in inputs)
            {
                var x = 0;
                foreach (var c in input.ToCharArray())
                {
                    if (c == '>')
                    {
                        grid.Add((x, y), SCDirection.East);
                    }

                    if (c == 'v')
                    {
                        grid.Add((x, y), SCDirection.South);
                    }

                    x++;
                }

                y++;
            }

            return grid;
        }
    }
}
