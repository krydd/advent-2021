using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day1
    {
        public void Part1()
        {
            var result = 0;
            var lastDepth = 0;

            foreach (var depth in File.ReadLines("Data\\day1.txt").Select(int.Parse))
            {
                if (lastDepth != 0 && lastDepth < depth)
                {
                    result++;
                }

                lastDepth = depth;
            }

            Console.WriteLine($"Ans: {result}");
        }

        public void Part2()
        {
            var sums = new List<int>();
            int a = 0, b = 0, c = 0;
            foreach (var depth in File.ReadLines("Data\\day1.txt").Select(int.Parse))
            {
                if (a == 0)
                {
                    a = depth;
                }
                else if (b == 0)
                {
                    b = depth;
                }
                else if (c == 0)
                {
                    c = depth;
                    sums.Add(a + b + c);
                }
                else
                {
                    a = b;
                    b = c;
                    c = depth;
                    sums.Add(a + b + c);
                }
            }

            var result = 0;
            var lastDepth = 0;

            foreach (var depth in sums)
            {
                if (lastDepth != 0 && lastDepth < depth)
                {
                    result++;
                }

                lastDepth = depth;
            }

            Console.WriteLine($"Ans: {result}");
        }
    }
}
