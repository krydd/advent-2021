using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day9
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day9.txt").ToList().Select(s => s.ToCharArray().Select(c => int.Parse(c.ToString())).ToList()).ToList();
            var height = inputs.Count;
            var width = inputs.First().Count;

            var grid = new int[height][];
            var count = 0;
            foreach (var row in inputs)
            {
                grid[count] = row.ToArray();
                count++;
            }

            var result = 0;
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var value = grid[y][x];
                    var neighbours = GetNeighbours(grid, x, y);
                    if (neighbours.All(n => n > value))
                    {
                        result += value + 1;
                    }
                }
            }

            Console.WriteLine($"Ans: {result}");
        }
        
        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day9.txt").ToList().Select(s => s.ToCharArray().Select(c => int.Parse(c.ToString())).ToList()).ToList();
            var height = inputs.Count;
            var width = inputs.First().Count;

            var grid = new int[height][];
            var count = 0;
            foreach (var row in inputs)
            {
                grid[count] = row.ToArray();
                count++;
            }

            var lowPoints = new List<(int x, int y)>();
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var value = grid[y][x];
                    var neighbours = GetNeighbours(grid, x, y);
                    if (neighbours.All(n => n > value))
                    {
                        lowPoints.Add((x, y));
                    }
                }
            }

            var topThreeBasins = new List<int>();
            foreach (var lowPoint in lowPoints)
            {
                var size = CalcBasinSize(lowPoint, grid, new List<(int, int)>());
                if (topThreeBasins.Count < 3)
                {
                    topThreeBasins.Add(size);
                }
                else if (topThreeBasins.OrderByDescending(i => i).Skip(2).First() < size)
                {
                    topThreeBasins = topThreeBasins.OrderByDescending(i => i).Take(2).Concat(new[] { size }).ToList();
                }
            }

            Console.WriteLine($"Ans: {topThreeBasins.Aggregate(1, (i1, i2) => i1*i2)}");
        }

        private int CalcBasinSize((int x, int y) point, int[][] grid, List<(int, int)> visited)
        {
            if (grid[point.y][point.x] == 9 || visited.Contains(point))
            {
                return 0;
            }
            
            visited.Add(point);
            
            var sum = 1;
            var height = grid.Length;
            var width = grid.First().Length;

            if (point.x - 1 >= 0)
            {
                sum += CalcBasinSize((point.x-1, point.y), grid, visited);
            }
            if (point.x + 1 < width)
            {
                sum += CalcBasinSize((point.x+1, point.y), grid, visited);
            }
            if (point.y - 1 >= 0)
            {
                sum += CalcBasinSize((point.x, point.y-1), grid, visited);
            }
            if (point.y + 1 < height)
            {
                sum += CalcBasinSize((point.x, point.y+1), grid, visited);
            }

            return sum;
        }

        private List<int> GetNeighbours(int[][] grid, int x, int y)
        {
            var neighbours = new List<int>();
            var height = grid.Length;
            var width = grid.First().Length;

            if (x - 1 >= 0)
            {
                neighbours.Add(grid[y][x-1]);
            }
            if (x + 1 < width)
            {
                neighbours.Add(grid[y][x+1]);
            }
            if (y - 1 >= 0)
            {
                neighbours.Add(grid[y-1][x]);
            }
            if (y + 1 < height)
            {
                neighbours.Add(grid[y+1][x]);
            }

            return neighbours;
        }
    }
}
