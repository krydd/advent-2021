using System;
using System.Collections.Generic;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day21
    {
        public void Part1()
        {
            var p1 = 7;
            var p2 = 8;
            var p1Score = 0;
            var p2Score = 0;

            var die = new DeterministicDie(100);

            var rolls = 0;
            var p1Turn = true;
            while (p1Score < 1000 && p2Score < 1000)
            {
                if (p1Turn)
                {
                    p1 = (p1 + die.RollAndSum(3)) % 10;
                    p1Score += p1 == 0 ? 10 : p1;
                    p1Turn = false;
                }
                else
                {
                    p2 = (p2 + die.RollAndSum(3)) % 10;
                    p2Score += p2 == 0 ? 10 : p2;
                    p1Turn = true;
                }

                rolls += 3;
            }

            Console.WriteLine($"Ans: {Math.Min(p1Score, p2Score) * rolls}");
        }

        private class DeterministicDie
        {
            private readonly int sides;
            private int current = 1;

            public DeterministicDie(int sides)
            {
                this.sides = sides;
            }

            public int RollAndSum(int times)
            {
                var sum = 0;
                for (var i = 0; i < times; i++)
                {
                    sum += current;
                    current++;
                    if (current > sides)
                    {
                        current = 1;
                    }
                }

                return sum;
            }
        }

        public void Part2()
        {
            var res = FindCombinationsForward((7, 8, 0, 0), new Dictionary<(int, int, int, int), (long, long)>());

            Console.WriteLine($"Ans: {Math.Max(res.Item1, res.Item2)}");
        }

        private (long, long) FindCombinationsForward((int p1, int p2, int s1, int s2) state, IDictionary<(int p1, int p2, int s1, int s2), (long, long)> cache)
        {
            if (state.s1 >= 21)
            {
                return (1, 0);
            }

            if (state.s2 >= 21)
            {
                return (0, 1);
            }

            if (cache.ContainsKey(state))
            {
                return cache[state];
            }

            var result = (0L, 0L);
            foreach (var roll in GetRolls())
            {
                var newP1Pos = (roll.Sum() + state.p1) % 10;
                var newP1Score = state.s1 + (newP1Pos == 0 ? 10 : newP1Pos);
                var (x1, y1) = FindCombinationsForward((state.p2, newP1Pos, state.s2, newP1Score), cache);
                result = (result.Item1 + y1, result.Item2 + x1);
            }

            cache.Add((state.p1, state.p2, state.s1, state.s2), result);

            return result;
        }

        private List<List<int>> GetRolls()
        {
            var rolls = new List<List<int>>
            {
                new() { 1, 1, 1 },
                new() { 1, 1, 2 },
                new() { 1, 1, 3 },
                new() { 1, 2, 1 },
                new() { 1, 2, 2 },
                new() { 1, 2, 3 },
                new() { 1, 3, 1 },
                new() { 1, 3, 2 },
                new() { 1, 3, 3 },
                new() { 2, 1, 1 },
                new() { 2, 1, 2 },
                new() { 2, 1, 3 },
                new() { 2, 2, 1 },
                new() { 2, 2, 2 },
                new() { 2, 2, 3 },
                new() { 2, 3, 1 },
                new() { 2, 3, 2 },
                new() { 2, 3, 3 },
                new() { 3, 1, 1 },
                new() { 3, 1, 2 },
                new() { 3, 1, 3 },
                new() { 3, 2, 1 },
                new() { 3, 2, 2 },
                new() { 3, 2, 3 },
                new() { 3, 3, 1 },
                new() { 3, 3, 2 },
                new() { 3, 3, 3 }
            };
            return rolls;
        }
    }
}
