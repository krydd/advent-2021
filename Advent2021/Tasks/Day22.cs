using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Advent2021.Tasks
{
    public class Day22
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day22.txt").ToList();

            var grid = new Dictionary<(int x, int y, int z), bool>();
            foreach (var input in inputs)
            {
                var (x1, x2, y1, y2, z1, z2) = GetRanges(input);
                var turnOn = input.StartsWith("on");

                for (var x = Math.Max(x1, -50); x <= Math.Min(x2, 50); x++)
                {
                    for (var y = Math.Max(y1, -50); y <= Math.Min(y2, 50); y++)
                    {
                        for (var z = Math.Max(z1, -50); z <= Math.Min(z2, 50); z++)
                        {
                            var coord = (x, y, z);

                            if (!turnOn && grid.ContainsKey(coord))
                            {
                                grid[coord] = false;
                            }
                            else if (turnOn)
                            {
                                if (grid.ContainsKey(coord))
                                {
                                    grid[coord] = true;
                                }
                                else
                                {
                                    grid.Add(coord, true);
                                }
                            }
                        }
                    }
                }
            }

            Console.WriteLine($"Ans: {grid.Count(entry => entry.Value)}");
        }

        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day22.txt").ToList();

            var boxes = new List<Box>();
            foreach (var input in inputs)
            {
                var (x1, x2, y1, y2, z1, z2) = GetRanges(input);
                var box = new Box((x1, x2), (y1, y2), (z1, z2), input.StartsWith("on"));

                boxes.AddRange(
                    boxes
                        .Select(storedBox => Intersection(box, storedBox))
                        .Where(intersectionBox => intersectionBox.IsValid)
                        .ToList());

                if (box.On)
                {
                    boxes.Add(box);
                }
            }

            Console.WriteLine($"Ans: {boxes.Sum(box => box.Size * (box.On ? 1 : -1))}");
        }

        private (int x1, int x2, int y1, int y2, int z1, int z2) GetRanges(string input)
        {
            var match = Regex.Match(input, @".*x=(.*),y=(.*),z=(.*)");
            var xRange = match.Groups[1].Value.Split("..").Select(int.Parse).ToArray();
            var yRange = match.Groups[2].Value.Split("..").Select(int.Parse).ToArray();
            var zRange = match.Groups[3].Value.Split("..").Select(int.Parse).ToArray();

            return (xRange[0], xRange[1], yRange[0], yRange[1], zRange[0], zRange[1]);
        }

        private Box Intersection(Box b1, Box b2)
        {
            return new Box(
                (Math.Max(b1.X.from, b2.X.from), Math.Min(b1.X.to, b2.X.to)),
                (Math.Max(b1.Y.from, b2.Y.from), Math.Min(b1.Y.to, b2.Y.to)),
                (Math.Max(b1.Z.from, b2.Z.from), Math.Min(b1.Z.to, b2.Z.to)),
                !b2.On
            );
        }

        private readonly record struct Box(
            (int from, int to) X,
            (int from, int to) Y,
            (int from, int to) Z,
            bool On
        )
        {
            public long Size => (X.to - X.from + 1L) * (Y.to - Y.from + 1L) * (Z.to - Z.from + 1L);

            public bool IsValid => X.from <= X.to && Y.from <= Y.to && Z.from <= Z.to;
        }
    }
}
