using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day16
    {
        public void Part1()
        {
            var input = File.ReadAllText("Data\\day16.txt");

            var binaryString = input.ToCharArray()
                .Select(c => c.ToString())
                .Aggregate("", (current, hex) => current + Convert.ToString(Convert.ToInt32(hex, 16), 2).PadLeft(4, '0'));

            var packages = ExtractPackages(binaryString, out _);

            Console.WriteLine($"Ans: {packages.Sum(p => p.VersionIncludingSub())}");
        }
        
        public void Part2()
        {
            var input = File.ReadAllText("Data\\day16.txt");

            var binaryString = input.ToCharArray()
                .Select(c => c.ToString())
                .Aggregate("", (current, hex) => current + Convert.ToString(Convert.ToInt32(hex, 16), 2).PadLeft(4, '0'));

            var packages = ExtractPackages(binaryString, out _);

            Console.WriteLine($"Ans: {packages.Single().Evaluate()}");
        }

        private List<Package> ExtractPackages(string binaryString, out int offset, int packageLimit = int.MaxValue)
        {
            var allPackages = new List<Package>();
            offset = -1;

            for (var i = 0; i < binaryString.Length; offset = i)
            {
                if (binaryString.Substring(i).All(c => c == '0'))
                {
                    break;
                }

                var currentPackage = new Package();

                currentPackage.Version = FromBinary(binaryString.Substring(i, 3));
                i += 3;

                currentPackage.Type = FromBinary(binaryString.Substring(i, 3));
                i += 3;

                if (currentPackage.Type == 4)
                {
                    var numberString = "";
                    while (true)
                    {
                        var part = binaryString.Substring(i, 5);
                        i += 5;
                        numberString += part[1..];
                        if (part.First() == '0')
                        {
                            break;
                        }
                    }

                    currentPackage.LiteralValue = FromBinary(numberString);

                    allPackages.Add(currentPackage);
                }
                else
                {
                    var lengthType = binaryString.Substring(i, 1);
                    i++;

                    if (lengthType == "0") // Nr of bits
                    {
                        var nrOfBits = (int)FromBinary(binaryString.Substring(i, 15));
                        i += 15;

                        currentPackage.SubPackages.AddRange(ExtractPackages(binaryString.Substring(i, nrOfBits), out _));
                        i += nrOfBits;
                    }
                    else // Nr of packages
                    {
                        var nrOfPackages = (int)FromBinary(binaryString.Substring(i, 11));
                        i += 11;

                        currentPackage.SubPackages.AddRange(ExtractPackages(binaryString.Substring(i), out var offsetToAdd, nrOfPackages));
                        i += offsetToAdd;
                    }

                    allPackages.Add(currentPackage);
                }

                if (allPackages.Count == packageLimit)
                {
                    offset = i;
                    return allPackages;
                }
            }

            return allPackages;
        }

        private long FromBinary(string s)
        {
            return Convert.ToInt64(s, 2);
        }

        private class Package
        {
            public long Version { get; set; }
            public long Type { get; set; }
            public long LiteralValue { get; set; }
            public List<Package> SubPackages { get; } = new();

            public long VersionIncludingSub()
            {
                return Version + SubPackages.Sum(p => p.VersionIncludingSub());
            }

            public long Evaluate()
            {
                return Type switch
                {
                    0 => SubPackages.Sum(p => p.Evaluate()), // sum
                    1 => SubPackages.Aggregate(1L, (product, p) => product * p.Evaluate()), // product
                    2 => SubPackages.Min(p => p.Evaluate()), // minimum
                    3 => SubPackages.Max(p => p.Evaluate()), // maximum
                    4 => LiteralValue, // value
                    5 => SubPackages[0].Evaluate() > SubPackages[1].Evaluate() ? 1 : 0, // greater than
                    6 => SubPackages[0].Evaluate() < SubPackages[1].Evaluate() ? 1 : 0, // less than
                    7 => SubPackages[0].Evaluate() == SubPackages[1].Evaluate() ? 1 : 0, // equal to
                    _ => throw new Exception($"Unknown type: {Type}")
                };
            }

            public override string ToString()
            {
                return Type switch
                {
                    0 => "sum",
                    1 => "product",
                    2 => "minimum",
                    3 => "maximum",
                    4 => "value",
                    5 => "greater than",
                    6 => "less than",
                    7 => "equal to",
                    _ => throw new Exception($"Unknown type: {Type}")
                } + $" ({SubPackages.Count})";
            }
        }
    }
}
