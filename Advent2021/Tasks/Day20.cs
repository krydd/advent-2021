using System;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day20
    {
        public void Part1And2()
        {
            var inputs = File.ReadLines("Data\\day20.txt").ToList();
            var algorithm = inputs[0].ToCharArray();

            var imageStrings = inputs.Skip(2).ToList();
            var width = imageStrings.First().Length;
            var height = imageStrings.Count;

            var image = new bool[width, height];
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    image[x, y] = imageStrings[y].ElementAt(x) == '#';
                }
            }

            const int steps = 50;
            
            var outside = false;
            for (var i = 0; i < steps; i++)
            {
                var newWidth = image.GetLength(0) + 2;
                var newHeight = image.GetLength(1) + 2;
                var newImage = new bool[newWidth, newHeight];
                for (var x = -1; x < newWidth - 1; x++)
                {
                    for (var y = -1; y < newHeight - 1; y++)
                    {
                        newImage[x + 1, y + 1] = AddFilter(algorithm, image, x, y, outside);
                    }
                }

                outside = algorithm[outside ? 511 : 0] == '#';
                image = newImage;
            }

            var sum = 0;
            for (var k = 0; k < image.GetLength(0); k++)
            {
                for (var l = 0; l < image.GetLength(1); l++)
                {
                    sum += image[k, l] ? 1 : 0;
                }
            }

            Console.WriteLine($"Ans: {sum}");
        }

        private bool AddFilter(char[] algorithm, bool[,] image, int x0, int y0, bool outside)
        {
            var maxX = image.GetLength(0);
            var maxY = image.GetLength(1);

            var bits = "";
            for (var y = y0 - 1; y <= y0 + 1; y++)
            {
                for (var x = x0 - 1; x <= x0 + 1; x++)
                {
                    if (x < 0 || x >= maxX || y < 0 || y >= maxY)
                    {
                        bits += outside ? "1" : "0";
                    }
                    else
                    {
                        bits += image[x, y] ? "1" : "0";
                    }
                }
            }
            
            var offset = Convert.ToInt32(bits, 2);
            return algorithm[offset] == '#';
        }
    }
}
