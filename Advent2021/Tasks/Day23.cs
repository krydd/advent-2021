using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day23
    {
        private const char Empty = '.';

        private readonly Dictionary<char, int> costs = new()
        {
            ['A'] = 1,
            ['B'] = 10,
            ['C'] = 100,
            ['D'] = 1000
        };

        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day23.txt").ToList();

            var caves = new char[4, 4];
            var hallway = new[] { Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty };

            var count = 0;
            foreach (var s in inputs.Skip(2).Take(4).Select(s => s.Trim().Trim('#')))
            {
                foreach (var letter in s.Split("#"))
                {
                    caves[count % 4, count / 4] = letter.First();
                    count++;
                }
            }

            var result = Move(hallway, caves, 0);

            Console.WriteLine($"Ans: {result}");
        }

        private int Move(char[] hallway, char[,] caves, int cost)
        {
            if (IsDone(caves))
            {
                return cost;
            }

            var moves = GetMoves(hallway, caves).ToList();
            if (!moves.Any())
            {
                return int.MaxValue;
            }

            return moves
                .Min(move => Move(move.h.ToArray(), move.c.Clone() as char[,], cost + move.cost));
        }

        private List<(char[] h, char[,] c, int cost)> GetMoves(char[] hallway, char[,] caves)
        {
            for (var i = 0; i < 4; i++) // Move inside ready cave
            {
                if (IsCaveReadyForEntry(caves, i, out var depth))
                {
                    var caveX = i * 2 + 2;
                    for (var x = caveX; x >= 0; x--)
                    {
                        if (hallway[x] == ToCaveSign(i))
                        {
                            var copies = Copy(hallway, caves);
                            copies.h[x] = Empty;
                            copies.c[i, depth - 1] = ToCaveSign(i);
                            return new List<(char[] h, char[,] c, int cost)> { (copies.h, copies.c, costs[ToCaveSign(i)] * (caveX - x + depth)) };
                        }

                        if (hallway[x] != Empty)
                        {
                            break;
                        }
                    }

                    for (var x = caveX; x < hallway.Length; x++)
                    {
                        if (hallway[x] == ToCaveSign(i))
                        {
                            var copies = Copy(hallway, caves);
                            copies.h[x] = Empty;
                            copies.c[i, depth - 1] = ToCaveSign(i);
                            return new List<(char[] h, char[,] c, int cost)> { (copies.h, copies.c, costs[ToCaveSign(i)] * (x - caveX + depth)) };
                        }

                        if (hallway[x] != Empty)
                        {
                            break;
                        }
                    }
                }
            }

            var moves = new List<(char[] h, char[,] c, int cost)>();
            var allowedHallwayPositions = new[] { 0, 1, 3, 5, 7, 9, 10 };

            for (var i = 0; i < 4; i++)
            {
                if (IsCaveDone(caves, i) || IsCaveReadyForEntry(caves, i, out _))
                {
                    continue;
                }

                for (var depth = 0; depth < 4; depth++)
                {
                    if (caves[i, depth] != Empty)
                    {
                        var caveX = i * 2 + 2;
                        for (var x = caveX; x >= 0; x--)
                        {
                            if (!allowedHallwayPositions.Contains(x))
                            {
                                continue;
                            }

                            if (hallway[x] != Empty)
                            {
                                break;
                            }

                            var copies = Copy(hallway, caves);
                            copies.h[x] = caves[i, depth];
                            copies.c[i, depth] = Empty;
                            moves.Add((copies.h, copies.c, costs[caves[i, depth]] * (depth + 1 + caveX - x)));
                        }
                        
                        for (var x = caveX; x < hallway.Length; x++)
                        {
                            if (!allowedHallwayPositions.Contains(x))
                            {
                                continue;
                            }

                            if (hallway[x] != Empty)
                            {
                                break;
                            }

                            var copies = Copy(hallway, caves);
                            copies.h[x] = caves[i, depth];
                            copies.c[i, depth] = Empty;
                            moves.Add((copies.h, copies.c, costs[caves[i, depth]] * (depth + 1 + x - caveX)));
                        }
                        
                        break;
                    }
                }
            }

            return moves;
        }

        private (char[] h, char[,] c) Copy(char[] hallway, char[,] caves)
        {
            return (hallway.ToArray(), caves.Clone() as char[,]);
        }

        private bool IsCaveReadyForEntry(char[,] caves, int cave, out int depth)
        {
            depth = -1;

            if (IsCaveDone(caves, cave))
            {
                return false;
            }

            for (var i = 3; i >= 0; i--)
            {
                if (caves[cave, i] == Empty)
                {
                    depth = i + 1;
                    return true;
                }
                
                if (caves[cave, i] != ToCaveSign(cave))
                {
                    return false;
                }
            }

            return false;
        }

        private char ToCaveSign(int cave)
        {
            return cave switch
            {
                0 => 'A',
                1 => 'B',
                2 => 'C',
                3 => 'D',
                _ => throw new ArgumentOutOfRangeException(nameof(cave), cave, null)
            };
        }

        private bool IsDone(char[,] caves)
        {
            for (var i = 0; i < 4; i++)
            {
                if (!IsCaveDone(caves, i))
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsCaveDone(char[,] caves, int cave)
        {
            for (var depth = 0; depth < 4; depth++)
            {
                if (caves[cave, depth] != ToCaveSign(cave))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
