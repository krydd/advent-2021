using System;
using System.IO;

namespace Advent2021.Tasks
{
    public class Day2
    {
        public void Part1()
        {
            var pos = 0;
            var depth = 0;

            var lines = File.ReadLines("Data\\day2.txt");
            
            foreach (var line in lines)
            {
                var split = line.Split(" ");
                var command = split[0];
                var value = int.Parse(split[1]);
                switch (command)
                {
                    case "forward":
                        pos += value;
                        break;
                    case "down":
                        depth += value;
                        break;
                    case "up":
                        depth -= value;
                        break;
                }
            }
            
            Console.WriteLine($"Ans: {pos * depth}");
        }
        
        public void Part2()
        {
            var pos = 0;
            var aim = 0;
            var depth = 0;

            var lines = File.ReadLines("Data\\day2.txt");
            
            foreach (var line in lines)
            {
                var split = line.Split(" ");
                var command = split[0];
                var value = int.Parse(split[1]);
                switch (command)
                {
                    case "forward":
                        pos += value;
                        depth += aim * value;
                        break;
                    case "down":
                        aim += value;
                        break;
                    case "up":
                        aim -= value;
                        break;
                }
            }
            
            Console.WriteLine($"Ans: {pos * depth}");
        }
    }
}
