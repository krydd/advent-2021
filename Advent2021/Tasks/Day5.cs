using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day5
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day5.txt").ToList();

            var lines = new List<Line>();
            foreach (var input in inputs)
            {
                var coords = input.Split(" -> ");
                var x1 = int.Parse(coords[0].Split(",")[0]);
                var y1 = int.Parse(coords[0].Split(",")[1]);

                var x2 = int.Parse(coords[1].Split(",")[0]);
                var y2 = int.Parse(coords[1].Split(",")[1]);

                if (x1 == x2 || y1 == y2)
                {
                    lines.Add(new Line((x1, y1), (x2, y2)));
                }
            }

            var grid = new Dictionary<(int, int), int>();
            foreach (var line in lines)
            {
                var x1 = line.StartPoint.x;
                var y1 = line.StartPoint.y;

                var x2 = line.EndPoint.x;
                var y2 = line.EndPoint.y;

                while (true)
                {
                    if (grid.ContainsKey((x1, y1)))
                    {
                        grid[(x1, y1)]++;
                    }
                    else
                    {
                        grid.Add((x1, y1), 1);
                    }

                    if (x1 == x2 && y1 == y2)
                    {
                        break;
                    }

                    x1 += x1 == x2 ? 0 : x1 < x2 ? 1 : -1;
                    y1 += y1 == y2 ? 0 : y1 < y2 ? 1 : -1;
                }
            }

            Console.WriteLine($"Ans: {grid.Values.Count(v => v > 1)}");
        }

        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day5.txt").ToList();

            var lines = new List<Line>();
            foreach (var input in inputs)
            {
                var coords = input.Split(" -> ");
                var x1 = int.Parse(coords[0].Split(",")[0]);
                var y1 = int.Parse(coords[0].Split(",")[1]);

                var x2 = int.Parse(coords[1].Split(",")[0]);
                var y2 = int.Parse(coords[1].Split(",")[1]);

                lines.Add(new Line((x1, y1), (x2, y2)));
            }

            var grid = new Dictionary<(int, int), int>();
            foreach (var line in lines)
            {
                var x1 = line.StartPoint.x;
                var y1 = line.StartPoint.y;

                var x2 = line.EndPoint.x;
                var y2 = line.EndPoint.y;

                while (true)
                {
                    if (grid.ContainsKey((x1, y1)))
                    {
                        grid[(x1, y1)]++;
                    }
                    else
                    {
                        grid.Add((x1, y1), 1);
                    }

                    if (x1 == x2 && y1 == y2)
                    {
                        break;
                    }

                    x1 += x1 == x2 ? 0 : x1 < x2 ? 1 : -1;
                    y1 += y1 == y2 ? 0 : y1 < y2 ? 1 : -1;
                }
            }

            Console.WriteLine($"Ans: {grid.Values.Count(v => v > 1)}");
        }

        private class Line
        {
            public (int x, int y) StartPoint { get; }
            public (int x, int y) EndPoint { get; }

            public Line((int, int) startPoint, (int, int) endPoint)
            {
                StartPoint = startPoint;
                EndPoint = endPoint;
            }
        }
    }
}
