using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day12
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day12.txt").ToList();
            var nodes = new List<Node>();

            foreach (var input in inputs)
            {
                var names = input.Split("-");
                var node1 = GetOrCreateNode(nodes, names[0], names[0].ToUpper() == names[0]);
                var node2 = GetOrCreateNode(nodes, names[1], names[1].ToUpper() == names[1]);
                node1.Connections.Add(node2);
                node2.Connections.Add(node1);
            }

            var start = nodes.Single(n => n.Name == "start");

            var result = Search(start, new List<string> { start.Name });

            Console.WriteLine($"Ans: {result}");
        }

        private int Search(Node node, List<string> visited)
        {
            if (node.Name == "end")
            {
                return 1;
            }

            var s = 0;
            var newVisited = visited.ToList();
            if (!node.LargeNode)
            {
                newVisited.Add(node.Name);
            }

            foreach (var connection in node.Connections.Where(c => !visited.Contains(c.Name)))
            {
                s += Search(connection, newVisited);
            }

            return s;
        }

        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day12.txt").ToList();
            var nodes = new List<Node>();

            foreach (var input in inputs)
            {
                var names = input.Split("-");
                var node1 = GetOrCreateNode(nodes, names[0], names[0].ToUpper() == names[0]);
                var node2 = GetOrCreateNode(nodes, names[1], names[1].ToUpper() == names[1]);
                node1.Connections.Add(node2);
                node2.Connections.Add(node1);
            }

            var start = nodes.Single(n => n.Name == "start");

            var result = Search2(start, new List<Node>());

            Console.WriteLine($"Ans: {result}");
        }

        private int Search2(Node node, List<Node> visited)
        {
            if (node.Name == "end")
            {
                return 1;
            }

            var s = 0;
            var newVisited = visited.ToList();
            newVisited.Add(node);

            foreach (var connection in node.Connections.Where(c => !c.IsVisited_Part2(newVisited)))
            {
                s += Search2(connection, newVisited);
            }

            return s;
        }

        private Node GetOrCreateNode(List<Node> nodes, string name, bool large)
        {
            var foundNode = nodes.FirstOrDefault(n => n.Name == name);
            if (foundNode != null)
            {
                return foundNode;
            }

            var newNode = new Node(name, large);
            nodes.Add(newNode);
            return newNode;
        }

        private class Node
        {
            public string Name { get; }

            public bool LargeNode { get; }

            public List<Node> Connections { get; } = new();

            public Node(string name, bool largeNode)
            {
                Name = name;
                LargeNode = largeNode;
            }

            public bool IsVisited_Part2(List<Node> visited)
            {
                if (LargeNode)
                {
                    return false;
                }

                if (Name is "start" or "end")
                {
                    return visited.Any(n => n.Name == Name);
                }

                var timesThisIsVisited = visited.Count(n => n.Name == Name);
                var twiceExists = visited.Where(n => !n.LargeNode).GroupBy(n => n).Any(grp => grp.Count() == 2);

                return twiceExists ? timesThisIsVisited > 0 : timesThisIsVisited > 1;
            }

            public override string ToString()
            {
                return $"{Name} ({(LargeNode ? "L" : "s")}): {Connections.Count}";
            }
        }
    }
}
