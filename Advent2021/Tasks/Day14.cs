using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Advent2021.Utils;

namespace Advent2021.Tasks
{
    public class Day14
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day14.txt").ToList();

            var template = inputs[0];
            var recipes = inputs.Skip(2)
                .Select(input => input.Split(" -> "))
                .ToDictionary(parts => parts[0], parts => parts[1]);

            const int steps = 10;
            for (var i = 0; i < steps; i++)
            {
                template = Substitute(template, recipes);
            }

            var counts = template.GroupBy(s => s).ToDictionary(grp => grp.Key, grp => grp.Count());

            Console.WriteLine($"Ans: {counts.Max(grp => grp.Value) - counts.Min(grp => grp.Value)}");
        }

        private string Substitute(string template, Dictionary<string, string> recipes)
        {
            var newTemplate = "";
            for (var i = 0; i < template.Length - 1; i++)
            {
                var pair = template.Substring(i, 2);
                if (!recipes.ContainsKey(pair))
                {
                    continue;
                }

                var betweenChar = recipes[pair];
                newTemplate += pair.ElementAt(0) + betweenChar;
            }

            return newTemplate + template.Last();
        }

        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day14.txt").ToList();

            var template = inputs[0];
            var recipes = inputs.Skip(2)
                .Select(input => input.Split(" -> "))
                .ToDictionary(parts => parts[0], parts => parts[1]);

            var combos = new Dictionary<string, long>();
            for (var i = 0; i < template.Length - 1; i++)
            {
                combos.AddOrUpdate(template.Substring(i, 2), 1);
            }

            const int steps = 40;

            for (var i = 0; i < steps; i++)
            {
                var newCombos = new Dictionary<string, long>();
                foreach (var pair in combos)
                {
                    if (recipes.ContainsKey(pair.Key))
                    {
                        var recipe = recipes[pair.Key];
                        var num = combos[pair.Key];
                        var combo1 = pair.Key.ElementAt(0) + recipe;
                        var combo2 = recipe + pair.Key.ElementAt(1);
                        newCombos.AddOrUpdate(combo1, num);
                        newCombos.AddOrUpdate(combo2, num);
                    }
                    else
                    {
                        newCombos.AddOrUpdate(pair.Key, pair.Value);
                    }
                }

                combos = newCombos;
            }

            var letterSums = new Dictionary<string, long>();
            foreach (var pair in combos)
            {
                letterSums.AddOrUpdate(pair.Key.ElementAt(0).ToString(), pair.Value);
            }

            var plusOne = template.ToCharArray().Last().ToString();
            letterSums[plusOne]++;

            var min = letterSums.Min(p => p.Value);
            var max = letterSums.Max(p => p.Value);

            Console.WriteLine($"Ans: {max - min}");
        }
    }
}
