using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day10
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day10.txt").ToList();

            var result = 0;
            foreach (var input in inputs)
            {
                var syntax = input.ToCharArray();
                var toFind = new List<char> { syntax.First() };
                for (var i = 1; i < syntax.Length; i++)
                {
                    var next = syntax[i];
                    if (toFind.Any() && next == GetEndChar(toFind.Last()))
                    {
                        toFind.RemoveAt(toFind.Count - 1);
                    }
                    else if (!IsEndChar(next))
                    {
                        toFind.Add(next);
                    }
                    else
                    {
                        result += ToScorePart1(next);
                        break;
                    }
                }
            }

            Console.WriteLine($"Ans: {result}");
        }

        private int ToScorePart1(char next)
        {
            return next switch
            {
                ')' => 3,
                ']' => 57,
                '}' => 1197,
                '>' => 25137,
                _ => throw new Exception($"Nope! '{next}'")
            };
        }

        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day10.txt").ToList();

            var scores = new List<long>();
            foreach (var input in inputs)
            {
                var syntax = input.ToCharArray();
                var toFind = new List<char> { syntax.First() };
                for (var i = 1; i < syntax.Length; i++)
                {
                    var next = syntax[i];
                    if (toFind.Any() && next == GetEndChar(toFind.Last()))
                    {
                        toFind.RemoveAt(toFind.Count - 1);
                    }
                    else if (!IsEndChar(next))
                    {
                        toFind.Add(next);
                    }
                    else
                    {
                        toFind.Clear();
                        break;
                    }
                }

                if (toFind.Any())
                {
                    toFind.Reverse();

                    var score = 0L;
                    foreach (var c in toFind.Select(GetEndChar))
                    {
                        score *= 5L;
                        score += ToScorePart2(c);
                    }

                    scores.Add(score);
                }
            }

            Console.WriteLine($"Ans: {scores.OrderBy(i => i).ToList()[scores.Count / 2]}");
        }

        private long ToScorePart2(char next)
        {
            return next switch
            {
                ')' => 1,
                ']' => 2,
                '}' => 3,
                '>' => 4,
                _ => throw new Exception($"Nope! '{next}'")
            };
        }

        private bool IsEndChar(char c)
        {
            return c is ')' or ']' or '}' or '>';
        }

        private char GetEndChar(char c)
        {
            return new Dictionary<char, char>
            {
                ['('] = ')',
                ['['] = ']',
                ['{'] = '}',
                ['<'] = '>'
            }[c];
        }
    }
}
