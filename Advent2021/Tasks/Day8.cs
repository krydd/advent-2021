using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day8
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day8.txt").Select(l => l.Split(" | ")[1]).ToList();

            var result = 0;
            foreach (var input in inputs)
            {
                result += input.Split(" ").Count(l => l.Length is 2 or 3 or 4 or 7);
            }

            Console.WriteLine($"Ans: {result}");
        }

        public void Part2()
        {
            var lines = File.ReadLines("Data\\day8.txt").ToList();
            var inputs = lines.Select(l => l.Split(" | ")[0]).ToList();
            var numbers = lines.Select(l => l.Split(" | ")[1]).ToList();

            var result = 0;
            for (var i = 0; i < inputs.Count; i++)
            {
                var input = inputs[i].Split(" ").Select(s => string.Concat(s.OrderBy(c => c))).ToList();
                var number = numbers[i].Split(" ").Select(s => string.Concat(s.OrderBy(c => c))).ToList();
                var decipher = BuildDecipher(input);

                result += int.Parse(string.Concat(number.Select(n => decipher[n])));
            }

            Console.WriteLine($"Ans: {result}");
        }

        private Dictionary<string, string> BuildDecipher(List<string> input)
        {
            var decipher = new Dictionary<int, string>
            {
                // Given
                [1] = input.Single(s => s.Length == 2),
                [7] = input.Single(s => s.Length == 3),
                [4] = input.Single(s => s.Length == 4),
                [8] = input.Single(s => s.Length == 7)
            };

            foreach (var fiveChar in input.Where(s => s.Length == 5))
            {
                var fourMinusOneChars = decipher[4].ToCharArray().Except(decipher[1].ToCharArray()).ToArray();
                if (string.Concat(fiveChar.ToCharArray().Intersect(decipher[1].ToCharArray())) == decipher[1])
                {
                    decipher[3] = fiveChar;
                }
                else if (string.Concat(fiveChar.ToCharArray().Intersect(fourMinusOneChars)) == string.Concat(fourMinusOneChars))
                {
                    decipher[5] = fiveChar;
                }
                else
                {
                    decipher[2] = fiveChar;
                }
            }

            foreach (var sixChar in input.Where(s => s.Length == 6))
            {
                if (string.Concat(sixChar.ToCharArray().Intersect(decipher[4].ToCharArray())) == decipher[4])
                {
                    decipher[9] = sixChar;
                }
                else if (string.Concat(sixChar.ToCharArray().Intersect(decipher[5].ToCharArray())) == decipher[5])
                {
                    decipher[6] = sixChar;
                }
                else
                {
                    decipher[0] = sixChar;
                }
            }

            return decipher.ToDictionary(entry => entry.Value, entry => entry.Key.ToString());
        }
    }
}
