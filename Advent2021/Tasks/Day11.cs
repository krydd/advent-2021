using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day11
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day11.txt").ToList().Select(s => s.ToCharArray().Select(c => int.Parse(c.ToString())).ToList()).ToList();
            var height = inputs.Count;

            var grid = new int[height][];
            var count = 0;
            foreach (var row in inputs)
            {
                grid[count] = row.ToArray();
                count++;
            }

            const int steps = 100;
            var flashes = SimulateGrid(steps, grid);

            Console.WriteLine($"Ans: {flashes}");
        }

        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day11.txt").ToList().Select(s => s.ToCharArray().Select(c => int.Parse(c.ToString())).ToList()).ToList();
            var height = inputs.Count;
            var width = inputs.First().Count;

            var grid = new int[height][];
            var count = 0;
            foreach (var row in inputs)
            {
                grid[count] = row.ToArray();
                count++;
            }

            var steps = 0;
            var found = false;
            while (!found)
            {
                steps++;
                SimulateGrid(1, grid);

                found = true;
                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        found = found && grid[y][x] == 0;
                    }
                }
            }

            Console.WriteLine($"Ans: {steps}");
        }

        private int SimulateGrid(int steps, int[][] grid)
        {
            var height = grid.Length;
            var width = grid.First().Length;
            var flashes = 0;
            for (var i = 0; i < steps; i++)
            {
                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        grid[y][x]++;
                    }
                }

                var cont = true;
                while (cont)
                {
                    cont = false;
                    for (var y = 0; y < height; y++)
                    {
                        for (var x = 0; x < width; x++)
                        {
                            if (grid[y][x] >= 10)
                            {
                                cont = true;
                                flashes++;

                                foreach (var neighbour in GetNeighbours(grid, x, y))
                                {
                                    grid[neighbour.y][neighbour.x]++;
                                    grid[y][x] = int.MinValue;
                                }
                            }
                        }
                    }
                }

                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        if (grid[y][x] < 0)
                        {
                            grid[y][x] = 0;
                        }
                    }
                }
            }

            return flashes;
        }

        private void PrintGrid(int[][] grid)
        {
            for (var y = 0; y < grid.Length; y++)
            {
                for (var x = 0; x < grid.First().Length; x++)
                {
                    Console.Write($"{grid[y][x]}");
                }

                Console.WriteLine("");
            }

            Console.WriteLine("----------");
        }

        private List<(int x, int y)> GetNeighbours(int[][] grid, int x, int y)
        {
            var neighbours = new List<(int, int)>();
            var height = grid.Length;
            var width = grid.First().Length;

            var left = x - 1 >= 0;
            var right = x + 1 < width;
            var top = y - 1 >= 0;
            var bottom = y + 1 < height;

            if (left)
            {
                neighbours.Add((x - 1, y));
            }

            if (right)
            {
                neighbours.Add((x + 1, y));
            }

            if (top)
            {
                neighbours.Add((x, y - 1));
            }

            if (bottom)
            {
                neighbours.Add((x, y + 1));
            }

            if (left && top)
            {
                neighbours.Add((x - 1, y - 1));
            }

            if (left && bottom)
            {
                neighbours.Add((x - 1, y + 1));
            }

            if (right && top)
            {
                neighbours.Add((x + 1, y - 1));
            }

            if (right && bottom)
            {
                neighbours.Add((x + 1, y + 1));
            }

            return neighbours;
        }
    }
}
