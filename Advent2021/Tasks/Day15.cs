using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent2021.Tasks
{
    public class Day15
    {
        public void Part1()
        {
            var inputs = File.ReadLines("Data\\day15.txt").Select(s => s.ToCharArray().Select(c => int.Parse(c.ToString())).ToList()).ToList();
            var height = inputs.Count;
            var width = inputs.First().Count;

            var grid = new int[height][];
            var count = 0;
            foreach (var row in inputs)
            {
                grid[count] = row.ToArray();
                count++;
            }

            var current = new Node(0, 0) { Risk = 0 };
            var nodes = new Dictionary<(int x, int y), Node> { [current.Point] = current };

            while (current != null)
            {
                var neighbours = GetNeighbours(current, height, width);
                foreach (var neighbour in neighbours)
                {
                    var riskFromCurrent = grid[neighbour.y][neighbour.x] + current.Risk;
                    if (nodes.ContainsKey(neighbour))
                    {
                        var newRisk = Math.Min(riskFromCurrent, nodes[neighbour].Risk);
                        nodes[neighbour].Risk = newRisk;
                    }
                    else
                    {
                        var newNode = new Node(neighbour.x, neighbour.y) { Risk = riskFromCurrent };
                        nodes.Add(neighbour, newNode);
                    }
                }

                current.Visited = true;
                current = nodes.Values.Where(n => !n.Visited).OrderBy(n => n.Risk).FirstOrDefault();
            }

            Console.WriteLine($"Ans: {nodes[(width - 1, height - 1)].Risk}");
        }
        
        public void Part2()
        {
            var inputs = File.ReadLines("Data\\day15.txt").Select(s => s.ToCharArray().Select(c => int.Parse(c.ToString())).ToList()).ToList();
            var height = inputs.Count;
            var width = inputs.First().Count;

            var grid = new int[height][];
            var count = 0;
            foreach (var row in inputs)
            {
                grid[count] = row.ToArray();
                count++;
            }

            height *= 5;
            width *= 5;

            var current = new Node(0, 0) { Risk = 0 };
            var nodes = new Dictionary<(int x, int y), Node> { [current.Point] = current };

            while (current != null)
            {
                var neighbours = GetNeighbours(current, height, width);
                foreach (var neighbour in neighbours)
                {
                    var riskFromCurrent = GetRiskLevelPart2(grid, neighbour.x, neighbour.y) + current.Risk;
                    if (nodes.ContainsKey(neighbour))
                    {
                        var newRisk = Math.Min(riskFromCurrent, nodes[neighbour].Risk);
                        nodes[neighbour].Risk = newRisk;
                    }
                    else
                    {
                        var newNode = new Node(neighbour.x, neighbour.y) { Risk = riskFromCurrent };
                        nodes.Add(neighbour, newNode);
                    }
                }

                current.Visited = true;
                current = nodes.Values.Where(n => !n.Visited).OrderBy(n => n.Risk).FirstOrDefault();
            }

            Console.WriteLine($"Ans: {nodes[(width - 1, height - 1)].Risk}");
        }

        private int GetRiskLevelPart2(int[][] grid, int x, int y)
        {
            var height = grid.Length;
            var width = grid.First().Length;

            var offsetX = x / width;
            var restX = x % width;
            
            var offsetY = y / height;
            var restY = y % height;

            var risk = grid[restY][restX] + offsetX + offsetY;
            if (risk > 9)
            {
                risk -= 9;
            }

            return risk;
        }

        private List<(int x, int y)> GetNeighbours(Node node, int height, int width)
        {
            var neighbours = new List<(int x, int y)>();
            var x = node.Point.x;
            var y = node.Point.y;

            if (x - 1 >= 0)
            {
                neighbours.Add((x - 1, y));
            }

            if (x + 1 < width)
            {
                neighbours.Add((x + 1, y));
            }

            if (y - 1 >= 0)
            {
                neighbours.Add((x, y - 1));
            }

            if (y + 1 < height)
            {
                neighbours.Add((x, y + 1));
            }

            return neighbours;
        }

        private class Node
        {
            public Node(int x, int y)
            {
                Point = (x, y);
                Visited = false;
                Risk = int.MaxValue;
            }

            public (int x, int y) Point { get; }

            public int Risk { get; set; }

            public bool Visited { get; set; }
        }
    }
}
