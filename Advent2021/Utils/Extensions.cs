using System.Collections.Generic;

namespace Advent2021.Utils
{
    public static class Extensions
    {
        public static void AddOrUpdate(this Dictionary<string, long> dictionary, string key, long toAdd)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary[key] += toAdd;
            }
            else
            {
                dictionary.Add(key, toAdd);
            }
        }
    }
}
